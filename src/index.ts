/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-empty-function */
declare const wx: any

interface MiniAppGlobal {
  request: (...args: any[]) => any
  getSystemInfoSync: (...args: any[]) => any

  // https://developers.weixin.qq.com/miniprogram/dev/api/base/app/app-event/wx.onUnhandledRejection.html
  onError?: (...args: any[]) => any
  onUnhandledRejection?: (...args: any[]) => any
  onPageNotFound?: (...args: any[]) => any
  onMemoryWarning?: (...args: any[]) => any
  getLaunchOptionsSync?: (...args: any[]) => any
}

const getMiniAppGlobal = (): MiniAppGlobal => {
  let aa = {}
  if (typeof wx === 'object') {
    aa = wx
  } else {
    throw new Error('平台获取失败')
  }

  return aa as MiniAppGlobal
}

type MiniAppPlatform = 'wechat' | 'unknown'

const getMiniAppPlatform = (): MiniAppPlatform => {
  let miniAppPlatform: MiniAppPlatform = 'unknown'
  if (typeof wx !== 'undefined') {
    miniAppPlatform = 'wechat'
  }

  return miniAppPlatform
}

const miniAppGlobal = getMiniAppGlobal()
const miniAppPlatform = getMiniAppPlatform()

export { miniAppGlobal, miniAppPlatform }
