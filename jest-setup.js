global.wx = {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  getSystemInfoSync() {
    return {
      model: 'iPhone XR',
      pixelRatio: 2,
      windowWidth: 414,
      windowHeight: 726,
      system: 'iOS 10.0.1',
      language: 'zh_CN',
      version: '7.0.4',
      screenWidth: 414,
      screenHeight: 896,
      SDKVersion: '2.14.0',
      brand: 'devtools',
      fontSizeSetting: 16,
      benchmarkLevel: 1,
      batteryLevel: 100,
      statusBarHeight: 44,
      safeArea: { right: 414, bottom: 896, left: 0, top: 44, width: 414, height: 852 },
      deviceOrientation: 'portrait',
      platform: 'devtools',
      devicePixelRatio: 2,
    }
  },
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  getLaunchOptionsSync() {
    return {}
  },
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
global.getCurrentPages = () => [
  {
    options: {
      __key_: '16049807753381',
    },
    route: 'pages/Home/Home',
  },
]
